# RT-Boot

RT-Boot是基于RT-Thread的bootloader系统，可用于引导Linux，初期主要支持ARM Cortex-A多核处理器；后续逐步过渡到在一些核上引导不同的操作系统，包括Linux及RT-Thread Smart等。